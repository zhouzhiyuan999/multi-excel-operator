﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private String previousDirec = null;
        private String mainKey = "";    //主表关键字所在列，也为最终表关键字
        private String subMainKey = "";     //副表关键字所在列 用作连接查询
        private CrFitness cf = null;
        private DataTable mainTable = null;
        private DataTable subTable = null;
        /// <summary>
        public Form1()
        {
            InitializeComponent();
            cf= new CrFitness(this);
            this.Load += new System.EventHandler(cf.Form_Load);
            this.SizeChanged += new System.EventHandler(cf.Form_SizeChanged);
        }

       //private void Form1_Load(object sender, EventArgs e)
       // {
       //     this.cf.GetInitialFormSize(this);
       //     this.cf.GetAllCrlLocation(this);
       //     this.cf.GetAllCrlSize(this);
       // }

  
      

        public DataSet getData()
        {
            //打开文件

            OpenFileDialog file = new OpenFileDialog();

            file.Filter = "Excel(*.xlsx)|*.xlsx|Excel(*.xls)|*.xls";
            if (previousDirec == null || previousDirec == "")
            {
                file.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }
            else
            {
                file.InitialDirectory = previousDirec;
            }


            file.Multiselect = false;

            if (file.ShowDialog() == DialogResult.Cancel)

                return null;

            //判断文件后缀

            var path = file.FileName;
            previousDirec = System.IO.Path.GetDirectoryName(path);
            string fileSuffix = System.IO.Path.GetExtension(path);

            if (string.IsNullOrEmpty(fileSuffix))
                return null;
            using (DataSet ds = new DataSet())

            {
                //判断Excel文件是2003版本还是2007版本

                string connString = "";

                if (fileSuffix == ".xls")

                    connString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + path + ";" + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";

                else

                    connString = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + path + ";" + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";

                //读取文件

                string sql_select = " SELECT * FROM [Sheet1$]";

                using (OleDbConnection conn = new OleDbConnection(connString))

                using (OleDbDataAdapter cmd = new OleDbDataAdapter(sql_select, conn))

                {
                    conn.Open();
                    cmd.Fill(ds);
                }
                if (ds == null || ds.Tables.Count <= 0) return null;
                return ds;
            }

        }


        //添加主表
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null; //每次打开清空内容
            dataGridView3.DataSource = null;
            try
            {
                DataSet ds = getData();
                if (ds != null)
                {
                    mainTable = ds.Tables[0];
                    DataTableUtil.initPrimaryKey(mainTable); //主表设置默认主键
                    mainKey = mainTable.PrimaryKey[0].ToString();
                    dataGridView1.DataSource = mainTable;
                    DataTableUtil.resetHeadStyle(dataGridView1);
              //      DataTableUtil.resetHeadStyle(dataGridView3);
                    //dataGridView1.SelectedRows[0].Cells[0].
                    //DataGridViewCellStyle style =dataGridView1.ColumnHeadersDefaultCellStyle;
                    //style.BackColor = Color.Navy;
                    //style.ForeColor = Color.White;
                    //style.Font = new Font(dataGridView1.Font, FontStyle.Bold);
                    dataGridView1.Columns[0].HeaderCell.Style.BackColor = SystemInfo.primaryKeyColor;
        //            dataGridView3.Columns[0].HeaderCell.Style.BackColor = SystemInfo.primaryKeyColor;
                }
        
            }
            catch(Exception exp)
            {
                Console.Write(exp.Message);
            }
    
        }

     
        //添加副表
        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = null; //每次打开清空内容

            try
            {
                DataSet ds = getData();
                if (ds != null)
                {
                     subTable = ds.Tables[0];
                    DataTableUtil.initPrimaryKey(subTable);
                    subMainKey = subTable.PrimaryKey[0].ToString();
                    dataGridView2.DataSource = subTable;
                    DataTableUtil.resetHeadStyle(dataGridView2);
                    dataGridView2.Columns[0].HeaderCell.Style.BackColor =SystemInfo.primaryKeyColor;
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }

        //主表设置主键
        private void button6_Click(object sender, EventArgs e)
        {
            //需要先检查要设置的列是否存在重复段
            DataTableUtil.resetHeadStyle(dataGridView1);
            mainKey=  DataTableUtil.setDGVPrimaryKey(dataGridView1);
          //  DataTableUtil.resetHeadStyle(dataGridView3);
          //  DataTableUtil.setDGVPrimaryKey(dataGridView3,mainKey);
        }

        //副表设置主键
        private void button7_Click(object sender, EventArgs e)
        {
            DataGridViewColumnHeaderCell head = dataGridView2.SelectedCells[0].OwningColumn.HeaderCell;
            if (DataTableUtil.isPrimaryKey(mainTable, head.Value.ToString())){
                DataTableUtil.resetHeadStyle(dataGridView2);
                subMainKey = DataTableUtil.setDGVPrimaryKey(dataGridView2);
            }
            else
            {
                MessageBox.Show("副表中的主键必须在主表中也存在");
            }

            


        }

        private void button3_Click(object sender, EventArgs e)
        {
            //将副表中的记录合并到主表中 结果显示在resultTable中，使用leftjoin

            HashSet<int> hs = new HashSet<int>();
            //获取附表中选中的行
            if (dataGridView2.SelectedCells.Count > 0)
            {
                foreach(DataGridViewCell cell in dataGridView2.SelectedCells)
                {
                    //获取选中的行
                    hs.Add(cell.RowIndex);
                }
            }else if (dataGridView2.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dataGridView2.SelectedRows)
                {
                    //获取选中的行
                    hs.Add(row.Index);
                }
            }
            int[] rows = hs.ToArray<int>();
            DataTable dt = (DataTable)dataGridView3.DataSource; 
            dt= DataTableUtil.LeftJoinRows(dt, mainTable, subTable,rows);
            dt=DataTableUtil.GetDistinctTable(dt);
            dataGridView3.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            DataTable result = DataTableUtil.UniteDataTable(mainTable, subTable, "result");
            dataGridView3.DataSource = result;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确认重置？", "清空表格", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ((DataTable)dataGridView3.DataSource).Clear();
            }        
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(ExcelUtil.ExportExcel("", dataGridView3, new SaveFileDialog())!=9999)
            {
                MessageBox.Show("导出成功！");
            }
        }
    }
}
