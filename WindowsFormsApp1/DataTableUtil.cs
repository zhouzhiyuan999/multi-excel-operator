﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class DataTableUtil
    {
        public static List<DataColumnModel> GetDataColumn(DataTable table)
        {
            List<DataColumnModel> list = null;
            DataColumnModel model = null;
            int id = -1;
            list = new List<DataColumnModel>();
            foreach (DataColumn dc in table.Columns)
            {
                id = id + 1;
                model = new DataColumnModel(dc.ColumnName, id);
                list.Add(model);
            }
            return list;
        } 


        public static void initPrimaryKey(DataTable table)
        {
            DataColumn[] clos = new DataColumn[1];
            clos[0] = table.Columns[0];
            table.PrimaryKey = clos;
        }

        public static void setPrimaryKey(DataTable table,String colName)
        {
            DataColumn[] clos = new DataColumn[1];
            clos[0] = table.Columns[colName];
            table.PrimaryKey = clos;
        }

        //重新设置表头样式
        public static void resetHeadStyle(DataGridView dgv)
        {
            foreach(DataGridViewColumn col in dgv.Columns)
            {
                col.HeaderCell.Style.BackColor= Color.Gray;
            }
        }

        public static Boolean isPrimaryKey(DataTable tb,String key)
        {
            try
            {
                foreach (DataColumn dc in tb.Columns)
                {
                    if (dc.ColumnName.Equals(key))
                    {
                        return true;
                    }
                }
                return false;
            }catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
           
        }


        public static String setDGVPrimaryKey(DataGridView dgv)
        {
            if (dgv.SelectedCells.Count ==1)
            {
                DataGridViewColumnHeaderCell head= dgv.SelectedCells[0].OwningColumn.HeaderCell;
                head.Style.BackColor = SystemInfo.primaryKeyColor;
                String mainKey = head.Value.ToString();
                DataTable source = ((DataTable)dgv.DataSource);
                setPrimaryKey(source, mainKey);
                return mainKey;
            }
            else
            {
                MessageBox.Show("请选择一列作为主键！");
            }
            return "";
        }

        public static String setDGVPrimaryKey(DataGridView dgv,String primaryKey)
        {
            if (dgv.SelectedCells.Count == 1)
            {
                DataGridViewColumnHeaderCell head = dgv.Columns[primaryKey].HeaderCell;
                head.Style.BackColor = SystemInfo.primaryKeyColor;
                String mainKey = head.Value.ToString();
                DataTable source = ((DataTable)dgv.DataSource);
                setPrimaryKey(source, primaryKey);
                return mainKey;
            }
            else
            {
                MessageBox.Show("请选择一列作为主键！");
            }
            return "";
        }


        /// <summary> 
        /// 将两个列不同(结构不同)的DataTable合并成一个新的DataTable 
        /// </summary> 
        /// <param name="DataTable1">表1</param> 
        /// <param name="DataTable2">表2</param> 
        /// <param name="DTName">合并后新的表名</param> 
        /// <returns>合并后的新表</returns> 
        public static DataTable UniteDataTable(DataTable table1, DataTable table2, string DTName) 
        {
            //克隆DataTable1的结构
            DataTable newDataTable = MergeTableWithNoData(null,table1, table2,true);

            //获取query查找的列名
            string[] strColumns = new string[table1.Columns.Count + table2.Columns.Count];
            for(int i=0;i< table1.Columns.Count; i++)
            {
                strColumns[i] = table1.Columns[i].ColumnName;
            }
            for(int i= 0; i < table2.Columns.Count; i++)
            {
                strColumns[i + table1.Columns.Count] = table2.Columns[i].ColumnName;
            }
            //将副表中的记录合并到主表中 结果显示在resultTable中，使用leftjoin
            var query = from m in table1.AsEnumerable()
                        join s in table2.AsEnumerable()
                        on m.Field<String>(table2.PrimaryKey[0].ColumnName) equals s.Field<String>(table2.PrimaryKey[0].ColumnName)
                        select m.ItemArray.Concat(s.ItemArray);
            try
            {
                foreach (var obj in query)
                {
                    DataRow dr = newDataTable.NewRow();
                    object[] item = obj.ToArray();
                    for (int i = 0; i < item.Length; i++)
                    {
                        int col = newDataTable.Columns[strColumns[i]].Ordinal;
                        if (dr.IsNull(col) )
                        {
                            dr[col] = item[i];
                        }
                    }
                    newDataTable.Rows.Add(dr);
                }
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
           

            //合并重复列
            //CombineDuplicateColumns(newDataTable);

            newDataTable.TableName = DTName; //设置DT的名字 
            return newDataTable;
        }


        public static DataTable MergeTableWithNoData(DataTable result,DataTable table1,DataTable table2,bool isCreate)
        {
            if (isCreate)
            {
                result= table1.Clone();
            }
            else
            {
                for (int i = 0; i < table1.Columns.Count; i++)
                {
                    //再向新表中加入DataTable2的列结构
                    if (!result.Columns.Contains(table1.Columns[i].ColumnName))
                    {
                        result.Columns.Add(table1.Columns[i].ColumnName);
                    }
                }
            }
            result.PrimaryKey = null;
            for (int i = 0; i < table2.Columns.Count; i++)
            {
                //再向新表中加入DataTable2的列结构
                if (!result.Columns.Contains(table2.Columns[i].ColumnName))
                {
                    result.Columns.Add(table2.Columns[i].ColumnName);
                }
            }
            return result;
        }


        public static DataTable LeftJoinRows(DataTable result, DataTable table1, DataTable table2, int[] rows)
        {
            if (table2.PrimaryKey.Count() != 1)
            {
                MessageBox.Show("请为副表设置一个主键！");
                return result;
            }
      
            result = MergeTableWithNoData(result,table1, table2, result == null);
            
            String candidateKey = table2.PrimaryKey[0].ColumnName;
            
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow dr2 = table2.Rows[rows[i]];
                DataRow[] dr1 = table1.Select(candidateKey + "='" + dr2[candidateKey] + "'");
                for(int j = 0; j < dr1.Length; j++)
                {
                    DataRow dr = result.NewRow();
                    object[] item = dr1[j].ItemArray;
                    for (int k = 0; k < item.Length; k++)
                    {
                        int col = result.Columns[table1.Columns[k].ColumnName].Ordinal;
                        if (dr.IsNull(col))
                        {
                            dr[col] = item[k];
                        }
                    }                 
                    for (int k = 0; k < dr2.ItemArray.Length; k++)
                    {
                        int col = result.Columns[table2.Columns[k].ColumnName].Ordinal;
                        if (dr.IsNull(col))
                        {
                            dr[col] = dr2.ItemArray[k];
                        }
                    }
                    result.Rows.Add(dr);
                }
            }
            return result;
        }
        public static DataTable GetDistinctTable(DataTable dtSource, params string[] columnNames)
        {
            DataTable distinctTable = dtSource.Clone();
            try
            {
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    DataView dv = new DataView(dtSource);
                    distinctTable = dv.ToTable(true, columnNames);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            return distinctTable;
        }

        public static DataTable GetDistinctTable(DataTable dtSource)
        {
            DataTable distinctTable = null;
            try
            {
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    string[] columnNames = GetTableColumnName(dtSource);
                    DataView dv = new DataView(dtSource);
                    distinctTable = dv.ToTable(true, columnNames);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            return distinctTable;
        }

        public static string[] GetTableColumnName(DataTable dt)
        {
            string cols = string.Empty;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                cols += (dt.Columns[i].ColumnName + ",");
            }
            cols = cols.TrimEnd(',');
            return cols.Split(',');
        }

    }


    class DataColumnModel
    {
        private String name;
        private int id;

        public DataColumnModel(String name,int id) {
            this.Name = name;
            this.Id = id;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
    }
}
